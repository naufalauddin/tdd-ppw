$('document').ready( function() {

    $('html').attr('data-theme', 'default');
    $(".color-change[color-value='default']").prop('checked', true);

    $('.color-change').click(function() {
        if ($('html').attr('data-theme') == $(this).attr('color-value')) {
            $(this).prop('checked', true);
        } else {
            $('html').attr('data-theme', $(this).attr('color-value'));
            $(this).siblings('input.color-change').prop('checked', false);
        }
    });
})