from django.test import TestCase, LiveServerTestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class AccordionTest(TestCase):
    def setUp(self):
        client = Client()
    
    def test_get_to_accordion_page(self):
        response = self.client.get('/accordion')

        self.assertTemplateUsed(response, 'accordion/accordion.html')

        self.assertContains(response, 'Activity')
        self.assertContains(response, 'Experience')
        self.assertContains(response, 'Achievement')

class AccordionFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url+'/accordion')

        super().setUp()

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_theme_change(self):
        
        theme_select = self.selenium.find_element_by_id('theme_change')

        theme_select.click()

        dark_theme = self.selenium.find_element_by_id('dark_theme_check')
        light_theme = self.selenium.find_element_by_id('light_theme_check')
        default_theme = self.selenium.find_element_by_id('default_theme_check')
