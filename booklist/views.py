from django.shortcuts import render, redirect
from django.http.response import JsonResponse
import requests

# Create your views here.
def index(request):
    return render(request, 'booklist/index.html')

def get_book(request, query):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q='+query)
    print(response)
    books = response.json()
    return JsonResponse(books)