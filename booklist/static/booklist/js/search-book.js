$("document").ready( function() {
    // before search query
    getBook("framework");

    // search bar functionality
    $("#search-bar").keyup( function(e) {
        if ($('#search-bar:focus') && e.keyCode === 13) {
            const search = $(this).val();
            getBook(search);
        }
    })
})

function getBook( book ) {
    $.ajax({
        url: "/book/get_book/"+book,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            
            const books = result.items;
            let table = "";
            
            for (let i = 0; i < books.length; i++) {
                table +="<tr>";

                const book = books[i].volumeInfo;

                table += `<td><a href="${book.infoLink}" target="_blank">${book.title}</a></td>`;


                if (book.authors == null) {
                    table += `<td>Penulis tidak terdaftar atau anonim</td>`;
                } else {
                    table += `<td>`;
                    for (let j = 0; j < book.authors.length; j++) {
                        if (j < book.authors.length-1) {
                            table += `${book.authors[j]},<br>`;
                        } else {
                            table += `${book.authors[j]}`;
                        }
                    }
                    table += `</td>`;
                }

                if (book.imageLinks == null) {
                    table += `<td>gambar tidak tersedia</td>`;
                } else {
                    table += `<td><img src=${book.imageLinks.thumbnail}></td>`;
                }
                table += "</tr>"
            }

            $("#search-result").html(table);
        }
    })
}