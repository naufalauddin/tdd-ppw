from django.urls import path

from . import views

app_name = 'booklist'

urlpatterns = [
    path('', views.index, name='index'),
    path('get_book/<str:query>', views.get_book)
]