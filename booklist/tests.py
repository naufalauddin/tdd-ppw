from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

# Create your tests here.
class BooklistTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_get_page(self):
        response = self.client.get('/book/')

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response=response, template_name='booklist/index.html')
        self.assertContains(response, 'search')

    def test_get_api(self):
        response = self.client.get('/book/get_book/ayam')

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'ayam')

class BookListFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url+'/book')

        super().setUp()

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_search_book(self):
        
        self.assertIn('Gambar', self.selenium.page_source)
        time.sleep(5)
        self.assertIn('Framework', self.selenium.page_source)

        search_bar = self.selenium.find_element_by_id('search-bar')

        search_bar.send_keys('buku')
        search_bar.send_keys(Keys.ENTER)

        time.sleep(5)

        self.assertIn('buku', self.selenium.page_source)