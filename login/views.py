from django.shortcuts import render, reverse, redirect
from django.contrib.auth import authenticate, login, logout as logout_auth
from django.contrib.auth.models import User
from .forms import LoginForm, RegistrationForm

# Create your views here.
def index(request):
    login_form = LoginForm()

    if request.user.is_authenticated:
        return redirect(reverse('login:landing'))

    if request.method == 'POST':

        login_form = LoginForm(request.POST)
        
        if login_form.is_valid():

            user = authenticate(
                request,
                username=login_form.cleaned_data['username'],
                password=login_form.cleaned_data['password']
            )

            if user is not None:
                login(request, user)
                return redirect(reverse('login:landing'))
            
            login_form.add_error(None, 'user not registered')
    
    return render(request, 'login/login.html', {'form': login_form})

def register(request):
    regis_form = RegistrationForm()

    if request.method == 'POST':

        regis_form = RegistrationForm(request.POST, request)

        if regis_form.is_valid():
            
            User.objects.create_user(
                username=regis_form.cleaned_data['username'],
                email=regis_form.cleaned_data['email'],
                first_name=regis_form.cleaned_data['first_name'],
                last_name=regis_form.cleaned_data['last_name'],
                password=regis_form.cleaned_data['password1'],
            )

            return redirect(reverse('login:index'))

    return render(request, 'login/register.html', {'form': regis_form})

def logout(request):
    logout_auth(request)
    return redirect(reverse('login:landing'))

def landing(request):
    return render(request, 'login/landing.html')