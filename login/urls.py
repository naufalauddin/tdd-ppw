from django.urls import path

from . import views

app_name = 'login'

urlpatterns = [
    path('', views.index, name='index'),
    path('landing', views.landing, name='landing'),
    path('register', views.register, name='register'),
    path('logout', views.logout, name='logout'),
]