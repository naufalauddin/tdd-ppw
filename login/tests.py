from django.test import TestCase, Client
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

# Create your tests here.
class AuthenticationTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user (
            username='nama_test', 
            email='test@test.com',
            first_name='Nama',
            last_name='Test',
            password='test'
        )

    def test_login_template(self):
        # user get to login page
        response = self.client.get(reverse('login:index'))
        
        # check template used
        self.assertTemplateUsed(response, 'login/login.html')
        
        # check template containind login
        self.assertContains(response, 'login')
    
    def test_user_login_redirect(self):
        self.client.login(username='nama_test', password='test')
        
        response = self.client.get(reverse('login:index'), follow=True)
        
        self.assertContains(response, 'Hello, Nama Test')
    
    def test_user_not_registered(self):
        response = self.client.post(reverse('login:index'), {'username': 'user', 'password': 'pass'})
        
        self.assertContains(response, 'Your username and password didn\'t matched')
    
    def test_user_login(self):
        
        response = self.client.post(reverse('login:index'), {
            'username': 'nama_test',
            'password':'test'
        }, follow=True)

        self.assertContains(response, 'Hello, Nama Test')
    
    def test_user_logout(self):
        self.client.login(username='nama_test', password='test')

        response = self.client.get(reverse('login:landing'))

        self.assertContains(response, 'Hello, Nama Test')

        response = self.client.get(reverse('login:logout'), follow=True)

        self.assertContains(response, 'Anda belum login')
        self.assertNotContains(response, 'Hello, Nama Test')
    
    def test_not_logged_in_user_logout(self):
        
        response = self.client.get(reverse('login:logout'), follow=True)

        self.assertContains(response, 'Anda belum login')
    
    def test_register_user(self):
        
        response = self.client.post(reverse('login:register'), {
            'username': 'test_user',
            'email': 'test@test.com',
            'password1': 'pass_test',
            'password2': 'pass_test',
            'first_name': 'Test',
            'last_name': 'User'
        })

        self.assertEqual(response.status_code, 302)

        self.client.login(username='test_user', password='pass_test')

        response = self.client.get(reverse('login:landing'))

        self.assertNotContains(response, 'Anda belum login')
        self.assertContains(response, 'Hello, Test User')
    
    def test_user_registration_not_complete(self):
        response = self.client.post(reverse('login:register'), {
            'username': 'test_user',
            'email': 'test',
            'password1': 'pass_test',
            'password2': 'pass_test',
            'first_name': 'Test',
            'last_name': 'User'
        })

        self.assertContains(response, 'Enter a valid email address')
        
        response = self.client.post(reverse('login:register'), {
            'username': 'test_user',
            'email': 'test@test.com',
            'password1': 'pass_test',
            'password2': 'pass_test1',
            'first_name': 'Test',
            'last_name': 'User'
        })

        self.assertContains(response, 'password fields didn&#39;t match')
        
        response = self.client.post(reverse('login:register'), {
            'username': 'nama_test',
            'email': 'test@test.com',
            'password1': 'pass_test',
            'password2': 'pass_test',
            'first_name': 'Test',
            'last_name': 'User'
        })

        self.assertContains(response, 'username already exists')