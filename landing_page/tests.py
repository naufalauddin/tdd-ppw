from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from django.utils import timezone
from .models import Status
from .forms import StatusForm

import time

class LandingTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    # test if arrived at landing page and its content is as expected
    def test_get_to_landing_page(self):
        # render landing page
        response = self.client.get('')

        # check if landing page is rendered and contains "Halo, apa Kabar?"
        self.assertContains(response, "Halo, apa kabar?")

    def test_status_models(self):
        date = timezone.now()
        status = Status(text="Status saya hari ini.")
        status.save()
        
        # check status text
        self.assertEqual(status.text, "Status saya hari ini.")

        # check status is saved into the database
        self.assertEqual(str(Status.objects.get(pk=1)), str(status))
    
    def test_status_valid_data(self):
        date = timezone.now()
        status = Status(post_time=date)
        status_form = StatusForm({
            'text': "Test Status"
        }, instance=status)
        
        # check if form is valid
        self.assertTrue(status_form.is_valid())

        # catch status and save it to database
        status = status_form.save()

        # check status text created from form 
        self.assertEqual(status.text, "Test Status")

        # check if status is in the database
        self.assertEqual(str(Status.objects.get(pk=1)), str(status))

    def test_status_blank_data(self):
        status_form = StatusForm({'post_time':timezone.now()})
        
        # check if form isn't valid
        self.assertFalse(status_form.is_valid())

        # check if form display an error
        self.assertEqual(status_form.errors, {
            'text': ['This field is required.']
        })
    
    def test_status_too_long(self):
        status_form = StatusForm({
            'text': 'This is a to much long paragraph for status as general so it shouldn\'t ' + 
                    'be acceptable by the system and should be considered invalid and not be ' +
                    'processed any further. Error should be raised from the form and everything ' +
                    'should be terminated as such.If this status pass the system and get posted, ' +
                    'there is something wrong in the system and you should check it immediately ' +
                    'and resolve the problem as fast as possible.'
        })

        # check if form isn't valid
        self.assertFalse(status_form.is_valid())

        # check if form display an error
        self.assertEqual(status_form.errors, {
            'text': ['There are more than 300 character']
        })
    
    def test_status_is_displayed(self):
        STATUS_TEXT = "this is status test"

        status = Status(text=STATUS_TEXT, post_time=timezone.now())
        status.save()

        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

        # check if status created just displayed to landing page
        self.assertContains(response, STATUS_TEXT)

class StatusLiveTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

        super(StatusLiveTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusLiveTestCase, self).tearDown()

    def test_status(self):
        selenium = self.selenium
        
        selenium.get(self.live_server_url)

        text_status = selenium.find_element_by_id('id_text')
        submit = selenium.find_element_by_id('status-submit')

        text_status.send_keys('Test Status')

        submit.send_keys(Keys.RETURN)

        self.assertIn('Test Status', selenium.page_source)
    
    def test_color_mode(self):
        selenium = self.selenium

        selenium.get(self.live_server_url)

        text_status = selenium.find_element_by_id('id_text')
        submit = selenium.find_element_by_id('status-submit')

        text_status.send_keys('Test Status')

        #status_wrapper = selenium.find_element_by_class_name('status')

        theme_change = selenium.find_element_by_id('theme_change')
        body = selenium.find_element_by_tag_name('html')

        self.assertEqual(body.value_of_css_property('background-color'), 'rgba(0, 145, 142, 1)')
        #self.assertEqual(status_wrapper.value_of_css_property, '#ffdc34')

        theme_select = self.selenium.find_element_by_id('theme_change')

        theme_select.click()

        dark_theme = self.selenium.find_element_by_id('dark_theme_check')
        light_theme = self.selenium.find_element_by_id('light_theme_check')
        default_theme = self.selenium.find_element_by_id('default_theme_check')

        # dark_theme.click()

        # self.assertEqual(body.value_of_css_property('background-color'), 'rgba(0, 69, 74, 1)')
        #self.assertEqual(status_wrapper.value_of_css_property, '#ed6363')

        # light_theme.click()

        # self.assertEqual(body.value_of_css_property('background-color'), 'rgba(0, 0, 0, 0)')
        #self.assertEqual(status_wrapper.value_of_css_property, '#fff')

        # default_theme.click()

        # self.assertEqual(body.value_of_css_property('background-color'), 'rgba(0, 145, 142, 1)')