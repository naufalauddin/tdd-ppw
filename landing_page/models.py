from django.db import models
from django.utils import timezone

class Status(models.Model):
    text = models.TextField(max_length=300)
    post_time = models.DateTimeField()

    def save(self, *args, **kwargs):
        ''' to compensate auto date created according to creation date '''
        if not self.id:
            self.post_time = timezone.now()
        
        return super(Status, self).save(*args, **kwargs)

    def __str__(self):
        text_string = self.text
        if len(str(text_string)) > 11:
            text_string = text_string[:11]+'...'
        return "<Status| pk: {}, text: '{}', post-time: {}>".format(self.id, text_string, self.post_time)