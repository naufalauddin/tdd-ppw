from django import forms
from django.forms import ModelForm, Textarea
from .models import Status

class StatusForm(ModelForm):

    class Meta:
        model = Status
        fields = ['text']
        error_messages = {
            'text': {
                'max_length': 'There are more than 300 character'
            }
        }
        widgets = {
            'text': Textarea(
                attrs={
                    'style': "min-width: 100%; max-width: 100%; min-height: 50px; max-height: 200px; border-radius: 10px; height: 100px; padding: 5px 10px; "
                    }
                )
        }