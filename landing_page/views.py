from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from .models import Status
from .forms import StatusForm

def index(request):
    
    statuses = Status.objects.all().order_by('-post_time')
    status_form = StatusForm()

    if request.method == 'POST':

        status_form = StatusForm(request.POST)

        if status_form.is_valid():
            
            status_form.save()

            return HttpResponseRedirect(reverse('landing_page:index'))  

    return render(request, 'landing_page/index.html', { 'statuses': statuses, 'status_form': status_form })